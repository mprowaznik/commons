package co.cosmose.core.exception;

public class CosmoseException extends RuntimeException {

    public CosmoseException() {
    }

    public CosmoseException(String message) {
        super(message);
    }

    public CosmoseException(String message, Throwable cause) {
        super(message, cause);
    }
}
